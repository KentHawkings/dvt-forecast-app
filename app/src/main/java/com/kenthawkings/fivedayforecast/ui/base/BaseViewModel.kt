package com.kenthawkings.fivedayforecast.ui.base

import android.content.Context
import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.kenthawkings.fivedayforecast.BR
import com.kenthawkings.fivedayforecast.interactor.Interactor

open class BaseViewModel<M, I : Interactor>(protected val interactor: I) : ViewModel(), Observable,
    LifecycleObserver {
    // public for testing
    var loading = false

    // public for testing
    var currentError: Throwable? = null

    var model: M? = null

    val loadingVisibility: Int
        @Bindable
        get() = if (loading) View.VISIBLE else View.GONE

    val contentVisibility: Int
        @Bindable
        get() = if (loading || (model == null && currentError != null)) View.GONE else View.VISIBLE

    val errorVisibility: Int
        @Bindable
        get() = if (currentError != null && model == null && !loading) View.VISIBLE else View.GONE

    @Transient
    private var mCallbacks: PropertyChangeRegistry? = null

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        synchronized(this) {
            if (mCallbacks == null) {
                mCallbacks = PropertyChangeRegistry()
            }
        }
        mCallbacks!!.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        synchronized(this) {
            if (mCallbacks == null) {
                return
            }
        }
        mCallbacks!!.remove(callback)
    }

    override fun onCleared() {
        super.onCleared()
        interactor.onClear()
    }

    open fun onClickReload(view: View) {
    }

    /**
     * Callback triggered when view is created
     */
    open fun onViewCreated(context: Context) {
    }

    /**
     * Callback triggered when view is destroyed
     *
     * Use to clean up memory
     */
    open fun onViewDestroyed() {
    }

    /**
     * Notifies listeners that all properties of this instance have changed.
     */
    fun notifyChange() {
        synchronized(this) {
            if (mCallbacks == null) {
                return
            }
        }
        mCallbacks!!.notifyCallbacks(this, 0, null)
    }

    /**
     * Notifies listeners that a specific property has changed. The getter for the property
     * that changes should be marked with [Bindable] to generate a field in
     * `BR` to be used as `fieldId`.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    fun notifyPropertyChanged(fieldId: Int) {
        synchronized(this) {
            if (mCallbacks == null) {
                return
            }
        }
        mCallbacks!!.notifyCallbacks(this, fieldId, null)
    }

    protected fun onContentLoaded() {
        notifyPropertyChanged(BR.loadingVisibility)
        notifyPropertyChanged(BR.contentVisibility)
        notifyPropertyChanged(BR.errorVisibility)
    }
}
