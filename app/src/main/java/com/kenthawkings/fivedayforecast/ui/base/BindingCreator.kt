package com.kenthawkings.fivedayforecast.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import java.io.Serializable

interface BindingCreator<B : ViewDataBinding> : Serializable {
    fun createBinding(inflater: LayoutInflater, viewGroup: ViewGroup?, attachedToParent: Boolean): B
}
