package com.kenthawkings.fivedayforecast.ui.forecast

import android.content.Context
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleObserver
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kenthawkings.fivedayforecast.R
import com.kenthawkings.fivedayforecast.data.LocationReceived
import com.kenthawkings.fivedayforecast.data.RequestLocation
import com.kenthawkings.fivedayforecast.interactor.ForecastInteractor
import com.kenthawkings.fivedayforecast.model.Forecast
import com.kenthawkings.fivedayforecast.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import timber.log.Timber

@HiltViewModel
class ForecastViewModel @Inject constructor(
    private val eventBus: EventBus,
    interactor: ForecastInteractor
) : BaseViewModel<Forecast, ForecastInteractor>(interactor), LifecycleObserver {
    var adapter: ForecastAdapter? = null

    var layoutManager: RecyclerView.LayoutManager? = null

    val currentWeatherDescription: String?
        get() = model?.items?.get(0)?.weather?.get(0)?.weatherType

    val maxTemp: String?
        get() = model?.items?.get(0)?.temperatureInfo?.maxTemp?.let {
            "$it°"
        }

    val minTemp: String?
        get() = model?.items?.get(0)?.temperatureInfo?.minTemp?.let {
            "$it°"
        }

    val currentTemp: String?
        get() = model?.items?.get(0)?.temperatureInfo?.currentTemp?.let {
            "$it°"
        }

    init {
        Timber.d("ViewModel created")
        loading = true
        eventBus.register(this)
    }

    override fun onViewCreated(context: Context) {
        super.onViewCreated(context)
        layoutManager = LinearLayoutManager(context)
        adapter = ForecastAdapter(model?.items)
    }

    override fun onClickReload(view: View) {
        super.onClickReload(view)
        eventBus.post(RequestLocation())
    }

    override fun onViewDestroyed() {
        super.onViewDestroyed()
        layoutManager = null
        adapter = null
    }

    public override fun onCleared() {
        super.onCleared()
        eventBus.unregister(this)
    }

    @get:ColorRes
    val backgroundColorRes
        get() = model?.items?.get(0)?.let {
            when {
                it.weather[0].weatherType.contains("cloud", true) -> R.color.cloudy
                it.weather[0].weatherType.contains("rain", true) -> R.color.rainy
                else -> R.color.sunny
            }
        }

    @ColorInt
    fun getBackgroundColor(view: View) = backgroundColorRes?.let {
        ContextCompat.getColor(view.context, it)
    }

    @get:DrawableRes
    val weatherImageRes
        get() = model?.items?.get(0)?.let {
            when {
                it.weather[0].weatherType.contains(
                    "cloud",
                    true
                ) -> R.drawable.forest_cloudy
                it.weather[0].weatherType.contains("rain", true) -> R.drawable.forest_rainy
                else -> R.drawable.forest_sunny
            }
        }

    fun getWeatherImage(view: View) = weatherImageRes?.let {
        ContextCompat.getDrawable(view.context, it)
    }

    @Subscribe
    @Suppress("unused")
    fun onLocationReceived(locationReceived: LocationReceived) {
        Timber.d("onLocationReceived: ${locationReceived.location}")
        val location = locationReceived.location
        val lat = location.latitude.toFloat()
        val lon = location.longitude.toFloat()
        loadModel(lat, lon)
    }

    fun loadModel(lat: Float, lon: Float) {
        interactor.loadForecast(lat, lon, {
            loading = false
            model = it
            adapter?.setList(model?.items)
            notifyChange()
        }) {
            Timber.e(it)
            loading = false
            currentError = it
            adapter?.setList(null)
            notifyChange()
        }
    }
}
