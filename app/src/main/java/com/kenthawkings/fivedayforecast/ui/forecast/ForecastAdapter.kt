package com.kenthawkings.fivedayforecast.ui.forecast

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BaseObservable
import androidx.recyclerview.widget.RecyclerView
import com.kenthawkings.fivedayforecast.BR
import com.kenthawkings.fivedayforecast.R
import com.kenthawkings.fivedayforecast.databinding.ListItemForecastBinding
import com.kenthawkings.fivedayforecast.model.ForecastItem
import java.util.Locale
import java.util.TreeMap
import kotlin.math.ceil
import org.joda.time.DateTime
import org.joda.time.LocalDate

class ForecastAdapter(list: List<ForecastItem>?) : RecyclerView.Adapter<ForecastItemViewHolder>() {
    private val items: TreeMap<LocalDate, MutableList<ForecastItem>> = TreeMap()

    init {
        initItems(list)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastItemViewHolder {
        val binding =
            ListItemForecastBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ForecastItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ForecastItemViewHolder, position: Int) {
        val currentItem =
            items[items.keys.toList()[position]]!!.sortedWith { o1, o2 ->
                ceil(o1.temperatureInfo.maxTemp.toFloat() - o2.temperatureInfo.maxTemp.toFloat()).toInt()
            }[0]

        val observable = ForecastItemObservable(holder.itemView.context, currentItem)
        holder.binding.setVariable(BR.observable, observable)
    }

    override fun getItemCount() = items.size

    fun setList(list: List<ForecastItem>?) {
        items.clear()
        initItems(list)
    }

    private fun initItems(list: List<ForecastItem>?) {
        list?.filter {
            // Ignore today's date
            DateTime(it.date * 1000).toLocalDate() != LocalDate.now()
        }?.forEach {
            val localDate = DateTime(it.date * 1000).toLocalDate()
            items[localDate]?.add(it) ?: run { items[localDate] = mutableListOf(it) }
        }
    }
}

class ForecastItemViewHolder(val binding: ListItemForecastBinding) :
    RecyclerView.ViewHolder(binding.root)

class ForecastItemObservable(context: Context, item: ForecastItem) :
    BaseObservable() {
    val weatherIcon: Drawable? = when {
        item.weather[0].weatherType.contains("cloud", true) ->
            AppCompatResources.getDrawable(context, R.drawable.ic_partly_sunny)
        item.weather[0].weatherType.contains("rain", true) ->
            AppCompatResources.getDrawable(context, R.drawable.ic_rain)
        else ->
            AppCompatResources.getDrawable(context, R.drawable.ic_clear)
    }

    val dayOfTheWeek: String = DateTime(item.date * 1000).dayOfWeek().getAsText(Locale.getDefault())

    val temperature = "${item.temperatureInfo.currentTemp}°"
}
