package com.kenthawkings.fivedayforecast.ui.forecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.kenthawkings.fivedayforecast.R
import com.kenthawkings.fivedayforecast.databinding.FragmentForecastBinding
import com.kenthawkings.fivedayforecast.ui.base.ViewModelFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForecastFragment : ViewModelFragment<ForecastViewModel, FragmentForecastBinding>() {
    override val viewModel: ForecastViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        val layout = view.findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar_main)
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar_main)
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        layout.setupWithNavController(toolbar, navController, appBarConfiguration)
        return view
    }
}
