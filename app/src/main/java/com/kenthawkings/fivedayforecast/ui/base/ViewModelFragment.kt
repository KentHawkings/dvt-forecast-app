package com.kenthawkings.fivedayforecast.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.kenthawkings.fivedayforecast.BR
import javax.inject.Inject

// XXX: Hopefully one day the Android entry points can support type parameters, then we never need
// to subclass this and we can just inject instances of ViewModelFragment directly. For now we make
// abstract to ensure subclassing
abstract class ViewModelFragment<VM : BaseViewModel<*, *>, B : ViewDataBinding> : Fragment() {
    abstract val viewModel: VM

    @Inject
    lateinit var bindingCreator: BindingCreator<B>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        lifecycle.addObserver(viewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = bindingCreator.createBinding(inflater, container, false)
        viewModel.onViewCreated(requireContext())
        binding.setVariable(BR.viewModel, viewModel)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.onViewDestroyed()
    }

    override fun onDetach() {
        super.onDetach()
        lifecycle.removeObserver(viewModel)
    }
}
