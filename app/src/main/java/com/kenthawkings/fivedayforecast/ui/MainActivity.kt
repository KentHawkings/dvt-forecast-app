package com.kenthawkings.fivedayforecast.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.tasks.CancellationTokenSource
import com.kenthawkings.fivedayforecast.R
import com.kenthawkings.fivedayforecast.data.LocationReceived
import com.kenthawkings.fivedayforecast.data.RequestLocation
import com.kenthawkings.fivedayforecast.databinding.ActivityMainBinding
import com.kenthawkings.fivedayforecast.extensions.hasCoarseLocationPermission
import com.kenthawkings.fivedayforecast.extensions.hasFineLocationPermission
import com.kenthawkings.fivedayforecast.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : BaseActivity(), ActivityCompat.OnRequestPermissionsResultCallback {
    companion object {
        const val RC_PLAY_SERVICES = 99
    }

    @Inject
    lateinit var fusedLocationProvider: FusedLocationProviderClient

    @Inject
    lateinit var eventBus: EventBus

    // TODO All of this location logic should probably be in the ForecastViewModel
    private val cancellationTokenSource by lazy { CancellationTokenSource() }

    // TODO could be a lot more graceful here. App shouldn't be killed when permission is denied,
    //  should rather show inline in UI
    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                getLocation()
            } else {
                AlertDialog.Builder(this, R.style.Theme_AppCompat_Dialog)
                    .setTitle(null).setMessage(R.string.permission_location_denied_explainer)
                    .setCancelable(false)
                    .setNeutralButton(
                        R.string.settings
                    ) { _, _ ->
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package", packageName, null)
                        intent.data = uri
                        startActivity(intent)
                    }
                    .setNegativeButton(
                        R.string.exit
                    ) { _, _ ->
                        finish()
                    }
                    .show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        Timber.d("onCreate")
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        eventBus.register(this)
        if (checkPlayServices()) {
            checkLocationPermission()
        }
    }

    override fun onStop() {
        super.onStop()
        eventBus.unregister(this)
        cancellationTokenSource.cancel()
    }

    @Subscribe
    @Suppress("unused")
    fun onRequestLocation(request: RequestLocation) {
        checkLocationPermission()
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, RC_PLAY_SERVICES)?.show()
            } else {
                Timber.i("This device is not supported.")
                finish()
            }
            return false
        }
        return true
    }

    private fun checkLocationPermission() {
        Timber.d("checkLocationPermission")
        when {
            hasFineLocationPermission || hasCoarseLocationPermission -> {
                Timber.d("location permission granted, retrieving location")
                getLocation()
            }
            else -> {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {
                    Timber.d("location permission not granted, showing rationale")
                    AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog)
                        .setMessage(R.string.permission_location_pre_explainer)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                        }
                        .show()
                } else {
                    Timber.d("location permission not granted, requesting")
                    permissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        Timber.d("getLocation")
        fusedLocationProvider.getCurrentLocation(
            LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY,
            cancellationTokenSource.token
        ).addOnSuccessListener {
            Timber.d("Location received: $it")
            it?.let {
                eventBus.post(LocationReceived(it))
            } ?: run {
                showErrorLocationDialog()
            }
        }.addOnFailureListener {
            showErrorLocationDialog()
        }
    }

    private fun showErrorLocationDialog() {
        AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog)
            .setMessage(R.string.error_location_not_found)
            .setPositiveButton(R.string.try_again) { _, _ ->
                getLocation()
            }
            .setNegativeButton(R.string.exit) { _, _ ->
                finish()
            }
            .show()
    }
}
