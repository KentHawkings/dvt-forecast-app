package com.kenthawkings.fivedayforecast.model

import com.google.gson.annotations.SerializedName

data class WeatherInfo(
    @SerializedName("main") val weatherType: String
)
