package com.kenthawkings.fivedayforecast.model

import com.google.gson.annotations.SerializedName

data class Forecast(
    @SerializedName("list") val items: List<ForecastItem>,
    val lastUpdated: Long? = null
)

data class ForecastItem(
    @SerializedName("weather") val weather: List<WeatherInfo>,
    @SerializedName("main") val temperatureInfo: TemperatureInfo,
    @SerializedName("dt") val date: Long
)
