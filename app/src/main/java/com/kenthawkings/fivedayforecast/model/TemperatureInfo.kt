package com.kenthawkings.fivedayforecast.model

import com.google.gson.annotations.SerializedName

data class TemperatureInfo(
    @SerializedName("temp") val currentTemp: String,
    @SerializedName("temp_min") val minTemp: String,
    @SerializedName("temp_max") val maxTemp: String
)
