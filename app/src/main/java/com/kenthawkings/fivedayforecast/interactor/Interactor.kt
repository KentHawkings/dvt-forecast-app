package com.kenthawkings.fivedayforecast.interactor

import androidx.annotation.CallSuper
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class Interactor(protected val disposables: CompositeDisposable = CompositeDisposable()) {
    @CallSuper
    fun onClear() {
        disposables.clear()
    }
}
