package com.kenthawkings.fivedayforecast.interactor

import com.kenthawkings.fivedayforecast.data.DataManager
import com.kenthawkings.fivedayforecast.extensions.initializeSchedulers
import com.kenthawkings.fivedayforecast.model.Forecast
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class ForecastInteractor(disposables: CompositeDisposable = CompositeDisposable()) :
    Interactor(disposables) {
    abstract fun loadForecast(
        lat: Float,
        lon: Float,
        onSuccess: (Forecast) -> Unit,
        onError: (Throwable) -> Unit
    )
}

class BaseForecastInteractor(
    private val dataManager: DataManager,
    disposables: CompositeDisposable = CompositeDisposable()
) : ForecastInteractor(disposables) {
    override fun loadForecast(
        lat: Float,
        lon: Float,
        onSuccess: (Forecast) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        disposables.add(
            dataManager.getForecast(lat, lon)
                .initializeSchedulers()
                .subscribe(onSuccess, onError)
        )
    }
}
