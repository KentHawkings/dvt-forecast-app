package com.kenthawkings.fivedayforecast.injection.module

import com.kenthawkings.fivedayforecast.data.DataManager
import com.kenthawkings.fivedayforecast.interactor.BaseForecastInteractor
import com.kenthawkings.fivedayforecast.interactor.ForecastInteractor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class ViewModelModule {
    @Provides
    @ViewModelScoped
    fun forecastInteractor(dataManager: DataManager): ForecastInteractor {
        return BaseForecastInteractor(dataManager)
    }
}
