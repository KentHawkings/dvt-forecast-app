package com.kenthawkings.fivedayforecast.injection.module

import android.app.Activity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
class ActivityModule {
    @Provides
    @ActivityScoped
    fun fusedLocationProvider(activity: Activity): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(activity)
    }
}
