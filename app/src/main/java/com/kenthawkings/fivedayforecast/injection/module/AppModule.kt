package com.kenthawkings.fivedayforecast.injection.module

import android.app.Application
import com.kenthawkings.fivedayforecast.EventBusIndex
import com.kenthawkings.fivedayforecast.R
import com.kenthawkings.fivedayforecast.data.DataManager
import com.kenthawkings.fivedayforecast.util.ConnectivityProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.greenrobot.eventbus.EventBus
import timber.log.Timber

// TODO: Might be worth creating a separate DataModule here. All of this networking stuff shouldn't be
//  accessible to all components
@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun eventBus(): EventBus = EventBus.builder().addIndex(EventBusIndex()).build()

    @Provides
    fun endpoint() = "https://api.openweathermap.org/data/2.5/"

    @Provides
    fun interceptor(app: Application) = Interceptor { chain ->
        val request = chain.request()
        val url = request.url.newBuilder()
            .addQueryParameter("appid", app.getString(R.string.openweather_api_key))
            .addQueryParameter("units", "metric")
            .build()
        chain.proceed(request.newBuilder().url(url).build())
    }

    @Provides
    fun client(interceptor: Interceptor) =

        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(HttpLoggingInterceptor { message ->
                println(message)
                Timber.tag("OkHttp").d(message)
            }.setLevel(HttpLoggingInterceptor.Level.BODY))
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()

    @Provides
    @Singleton
    fun dataManager(endpoint: String, client: OkHttpClient): DataManager {
        return DataManager(endpoint, client)
    }

    @Provides
    @Singleton
    fun connectivityProvider(app: Application): ConnectivityProvider {
        return ConnectivityProvider.create(app)
    }
}
