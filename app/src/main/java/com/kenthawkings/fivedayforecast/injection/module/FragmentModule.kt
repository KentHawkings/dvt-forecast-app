package com.kenthawkings.fivedayforecast.injection.module

import android.view.LayoutInflater
import android.view.ViewGroup
import com.kenthawkings.fivedayforecast.databinding.FragmentForecastBinding
import com.kenthawkings.fivedayforecast.ui.base.BindingCreator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
class FragmentModule {
    @Provides
    fun forecastBindingCreator(): BindingCreator<FragmentForecastBinding> {
        return object : BindingCreator<FragmentForecastBinding> {
            override fun createBinding(
                inflater: LayoutInflater,
                viewGroup: ViewGroup?,
                attachedToParent: Boolean
            ): FragmentForecastBinding {
                return FragmentForecastBinding.inflate(inflater, viewGroup, attachedToParent)
            }
        }
    }
}
