package com.kenthawkings.fivedayforecast.extensions

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

fun <T> Single<T>.initializeSchedulers(
    subscribeScheduler: Scheduler = Schedulers.io(),
    unsubscribeScheduler: Scheduler = Schedulers.io(),
    observeScheduler: Scheduler = AndroidSchedulers.mainThread()
): Single<T> {
    return this.subscribeOn(subscribeScheduler)
        .unsubscribeOn(unsubscribeScheduler)
        .observeOn(observeScheduler)
}
