package com.kenthawkings.fivedayforecast.data.remote

import com.kenthawkings.fivedayforecast.model.Forecast
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("forecast")
    fun getFiveDayForecast(
        @Query("lat") lat: Float,
        @Query("lon") lon: Float
    ): Single<Forecast>
}
