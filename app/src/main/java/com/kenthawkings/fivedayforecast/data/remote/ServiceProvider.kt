package com.kenthawkings.fivedayforecast.data.remote

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ServiceProvider(endpoint: String, client: OkHttpClient) {
    val weatherService: WeatherService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(endpoint)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(client)
            .build()
        weatherService = retrofit.create(WeatherService::class.java)
    }
}
