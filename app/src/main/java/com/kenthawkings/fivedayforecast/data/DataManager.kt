package com.kenthawkings.fivedayforecast.data

import com.kenthawkings.fivedayforecast.data.remote.ServiceProvider
import com.kenthawkings.fivedayforecast.model.Forecast
import io.reactivex.rxjava3.core.Single
import okhttp3.OkHttpClient

class DataManager(endpoint: String, client: OkHttpClient) {
    private val services = ServiceProvider(endpoint, client)

    fun getForecast(lat: Float, lon: Float): Single<Forecast> {
        return services.weatherService.getFiveDayForecast(lat, lon)
    }
}
