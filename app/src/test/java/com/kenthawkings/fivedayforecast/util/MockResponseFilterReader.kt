package com.kenthawkings.fivedayforecast.util

import java.io.InputStreamReader

class MockResponseFileReader(name: String) {
    val content: String

    init {
        val reader = InputStreamReader(this.javaClass.classLoader!!.getResourceAsStream(name))
        content = reader.readText()
        reader.close()
    }
}
