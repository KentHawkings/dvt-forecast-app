package com.kenthawkings.fivedayforecast.util

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future
import java.util.concurrent.FutureTask
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors

class CurrentThreadExecutor : ExecutorService {
    override fun execute(command: Runnable?) {
        command?.run()
    }

    override fun shutdown() {}

    override fun shutdownNow() = mutableListOf<Runnable>()

    override fun isShutdown() = false

    override fun isTerminated() = false

    override fun awaitTermination(timeout: Long, unit: TimeUnit?) = false

    override fun <T : Any?> submit(task: Callable<T>?) = FutureTask(task).apply {
        run()
    }

    override fun <T : Any?> submit(task: Runnable?, result: T) = FutureTask(task, result).apply {
        run()
    }

    override fun submit(task: Runnable?) = FutureTask(task, null).apply {
        run()
    }

    override fun <T : Any?> invokeAll(tasks: MutableCollection<out Callable<T>>?): MutableList<FutureTask<T>>? =
        tasks?.stream()?.map { this.submit(it) }?.collect(Collectors.toList())

    override fun <T : Any?> invokeAll(
        tasks: MutableCollection<out Callable<T>>?,
        timeout: Long,
        unit: TimeUnit?
    ): MutableList<Future<T>>? =
        tasks?.stream()?.map { this.submit(it) }?.collect(Collectors.toList())

    override fun <T : Any?> invokeAny(tasks: MutableCollection<out Callable<T>>?): T? =
        tasks?.stream()?.map { this.submit(it) }?.findFirst()?.get()?.get()

    override fun <T : Any?> invokeAny(
        tasks: MutableCollection<out Callable<T>>?,
        timeout: Long,
        unit: TimeUnit?
    ): T? =
        tasks?.stream()?.map { this.submit(it) }?.findFirst()?.get()?.get()
}
