package com.kenthawkings.fivedayforecast

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kenthawkings.fivedayforecast.data.DataManager
import com.kenthawkings.fivedayforecast.interactor.BaseForecastInteractor
import com.kenthawkings.fivedayforecast.ui.forecast.ForecastViewModel
import com.kenthawkings.fivedayforecast.util.CurrentThreadExecutor
import com.kenthawkings.fivedayforecast.util.MockResponseFileReader
import com.kenthawkings.fivedayforecast.util.RxImmediateSchedulerRule
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.greenrobot.eventbus.EventBus
import org.joda.time.DateTimeZone
import org.joda.time.tz.UTCProvider
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock

@RunWith(MockitoJUnitRunner::class)
class ForecastViewModelTest {
    @get:Rule
    val testInstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testRxImmediateSchedulerRule = RxImmediateSchedulerRule()

    private lateinit var mockServer: MockWebServer

    private lateinit var viewModel: ForecastViewModel

    @Before
    fun setUp() {
        mockServer = MockWebServer()
        mockServer.start()

        val eventBus = mock<EventBus>()

        val client = OkHttpClient.Builder()
            .dispatcher(Dispatcher(CurrentThreadExecutor()))
            .build()

        val dataManager = DataManager(mockServer.url("/").toString(), client)
        val interactor = BaseForecastInteractor(dataManager)

        viewModel = ForecastViewModel(eventBus, interactor)
        DateTimeZone.setProvider(UTCProvider())
    }

    @Test
    fun testViewModel() {
        val reader = MockResponseFileReader("forecast_success_response.json")
        assertNotNull(reader.content)

        mockServer.enqueue(MockResponse().apply {
            setResponseCode(200)
            setBody(reader.content)
            setHeader("content-type", "application/json")
        })

        viewModel.loadModel(0F, 0F)

        assert(!viewModel.loading)
        assertNull(viewModel.currentError)
        assertNotNull(viewModel.model)

        assertEquals(viewModel.currentWeatherDescription, "Clouds")
        assertEquals(viewModel.maxTemp, "19.64°")
        assertEquals(viewModel.minTemp, "16.87°")
        assertEquals(viewModel.currentTemp, "19.64°")

        assertEquals(viewModel.backgroundColorRes, R.color.cloudy)
        assertEquals(viewModel.weatherImageRes, R.drawable.forest_cloudy)

        viewModel.onViewCreated(mock())

        assertNotNull(viewModel.layoutManager)
        assertNotNull(viewModel.adapter)
        assert(viewModel.adapter!!.itemCount == 5)

        // TODO test adapter items somehow
    }

    @After
    fun tearDown() {
        mockServer.close()
    }
}
